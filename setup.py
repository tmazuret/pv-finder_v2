import setuptools

setuptools.setup(
    name="pv-finder_v2",
    version="0.0.1",
    description="Refactoization of PV-finder",
    packages=setuptools.find_packages(),
    python_requires='>=3.9',
)
