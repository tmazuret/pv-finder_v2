import sys
import os
import pprint

import pandas as pd
import numpy as np
import torch

from utils.utilities import from_bins_to_z
from utils.plotting_utils import plot_z_hist
from utils.efficiency_utils import ValueSet, eval_efficiency

## ============================================================
def test_model(model, test_data, configs, weights_file, outPutFolder_plots, nEvts_display=10, display=True):
        

    ## =================================================================
    ## We want to look into events and within each event the intervals
    ## Since the data is structured as an array of intervals, we 
    ## actually need to reconstruct the event by adding up each 
    ## corresponding interval.
    ## =================================================================

    ## -----------------------------------------------------------------
    ## Get the considered experiement: LHCb / ATLAS / CMS...
    Exp = configs['Experiment']
    
    ## -----------------------------------------------------------------
    ## Get the global configuration 
    global_configs = configs['global_configs'][Exp]
    
    ## -----------------------------------------------------------------
    ## Let start by getting back the number of intervals per event    
    nBinsKDE_perEvt     = global_configs["n_bins_poca_kde"]
    nBinsPerInterval    = global_configs["nBinsPerInterval"]
    nIntervals_perEvt   = int(nBinsKDE_perEvt/nBinsPerInterval)    
    
    ## =================================================================
    ## First get the model outputs (from inputs) and the labels from the test data
    ## =================================================================

    ## ----------------------------------------------------------------
    ## Get the model weights (if not using test_model after a training)
    weights = torch.load(weights_file)
    model.load_state_dict(weights)
    
    ## ----------------------------------------------------------------
    ## Swich the model to evaluation mode and the model to 'cpu'
    model.to('cpu')
    model.eval()

    ## =================================================================
    ## EFFICIENCY EVALUATION
    ## =================================================================
    if configs["model_class"]=="tracks-to-hist" or configs["model_class"]=="KDE-to-hist":

        ## ----------------------------------------------------------------
        ## Get the efficiency parameters dictionnary from the config file
        eff_params = configs["efficiency_config"]
        
        ## --------------------------------------------------------
        ## Reset the efficiency values 
        eff = ValueSet(0, 0, 0, 0)
        
        ## ----------------------------------------------------------------
        ## Splitting the inputs and outputs is usefull regarding to memory allocation...
        with torch.no_grad():
            ## Get the full test data for efficiency calculation
            model_preds_eff = model(test_data.dataset.tensors[0]).cpu().numpy()
            labels_eff      = test_data.dataset.tensors[1].cpu().numpy()

            ## Total number of available events in the test data
            totalEvts_test_data = int(len(labels_eff)/nIntervals_perEvt)

            ## If doing intervals, reshape the data to be shaped as events
            # Start by computing the number of intervals per event
            if configs["training_configs"]["doIntervals"]:
                model_preds_eff = model_preds_eff.reshape((totalEvts_test_data,nBinsKDE_perEvt))
                labels_eff      = labels_eff.reshape((totalEvts_test_data,nBinsKDE_perEvt))
                
            for iEvt in range(totalEvts_test_data):                
                eff += eval_efficiency(labels_eff[iEvt], model_preds_eff[iEvt], **eff_params)
            
            ## Printing out efficiency results and parameters used for the evaluation
            print("*"*100)
            print(" Model efficiency on test data:\n",eff.pretty())
            print("")
            print("-"*100)
            print("")
            print(" Obtained with efficiency parameters:")
            pprint.pprint(eff_params, indent=10)
            print("*"*100)
            
    ## =================================================================
    ## PLOTTING PREDICTIONS AND LABELS
    ## =================================================================
    ## ----------------------------------------------------------------
    ## Splitting the inputs and outputs is usefull regarding to memory allocation...
    with torch.no_grad():
        inputs = test_data.dataset.tensors[0]
        labels = test_data.dataset.tensors[1]
                
        nSplit = []
        for ii in range(20):
            nSplit.append((ii+1)*100000)

        inputs_Split = torch.tensor_split(inputs, nSplit, dim=0)    
        labels_Split = torch.tensor_split(labels, nSplit, dim=0)
        
        defaultSplitSize = inputs_Split[0].shape[0]


    with torch.no_grad():
        ## Loop over the tensors stored in the split tuple of tensors
        for iChunk in range(1):
            
            labels      = labels_Split[iChunk].cpu().numpy()
            model_preds = model(inputs_Split[iChunk]).cpu().numpy()
    
    # model_pred = np.asarray(model_preds[0])
    
            
        
    ## Set the bin values for each interval (by definition from 0 to nBinsPerInterval)
    interval_bins      = np.arange(nBinsPerInterval)
    ## Set the bin values for each event (by definition from 0 to nBinsTotal)
    event_bins         = np.arange(nBinsKDE_perEvt)
    ## Get the z values for the current interval
    z_vals_evt         = from_bins_to_z(event_bins, global_configs)
    ## -----------------------------------------------------------------
    ## Now we loop over an arbitrary number of events defined as an 
    ## argument of test_model
    listOfEvents = np.arange(nEvts_display)
    for jj in listOfEvents:
        
        if display:
            print("*"*100)
            print("")
            print(" Event (%s) "%jj)
            print("")

        ## ----------------------------------------------
        ## Define empty label and model prediction
        ## that will be filled within the loop over
        ## the intervals (see below)
        event_label      = np.asarray([])
        event_model_pred = np.asarray([])
        
        ## ----------------------------------------------
        ## Let's now loop over the global intervals ID  
        ## and retreive the labels and model predictions
        ## Set the interval ID within each event to zero
        interval_thisEvt = 0        
        for interval_global in range(jj*nIntervals_perEvt,jj*nIntervals_perEvt+nIntervals_perEvt):
            label      = np.asarray(labels[interval_global])
            model_pred = np.asarray(model_preds[interval_global])
            
            ## Get the z values for the current interval
            z_vals = from_bins_to_z(interval_bins, global_configs, True, interval_thisEvt, nBinsPerInterval)
            
            ## ----------------------------------------------
            ## We concatenate all intervals within one event (jj) 
            event_label           = np.concatenate((event_label,label))
            event_model_pred      = np.concatenate((event_model_pred,model_pred))
            
            ## ----------------------------------------------
            ## ymax is meant to define the maximum of the plot
            ## where both the model prediction and label
            ## will be superimposed
            ymax = max(np.max(label),np.max(model_pred))
            ## ----------------------------------------------
            ## Only make the interval plot if ymax is above a certain threshold
            ## as most of the intervals are empty: i.e. we only want ot check the ones 
            ## in where there is some activity
            if (ymax>0.1):
                if display:
                    print("="*50)
                    print(" Event (%s) -- Interval (%s) -- ymax (%5.2f) " %(jj,interval_thisEvt,ymax))
                ## For protting purposes let's redefine ymax
                ymax = max(ymax,1.0)
                plot_tag = str(outPutFolder_plots / f'{configs["model_class"]}_Evt{jj}_Inter{interval_thisEvt}')
                plot_z_hist(ymax, z_vals, label, model_pred, configs, plot_tag, display)
                
            ## Iterate the interval id within each event    
            interval_thisEvt += 1
            
        ## ----------------------------------------------
        ## Finally we plot the event histogram
        label_max      = max(1.1*np.max(event_label),1.0)
        model_pred_max = max(1.1*np.max(event_model_pred),1.0)
        ymax = max(label_max,model_pred_max)
        plot_tag = str(outPutFolder_plots / f'{configs["model_class"]}_Evt{jj}__AllInter')
        plot_z_hist(ymax, z_vals_evt, event_label, event_model_pred, configs, plot_tag, display)
        if display:
            print("")
            print("*"*100)
            print("")

    ## ================================================================================
    # Save training plots
    print("")
    print("*"*100)
    print("Saved testing plots in:")
    print("")
    print(outPutFolder_plots)
    print("")
    print("*"*100)
    
    ## ----------------------------------------------------------------
    ## Swich the model back to 'cuda'
    model.to('cuda')
        