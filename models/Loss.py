#!/usr/bin/env python3
'''
Definitions of the different loss functions used for the different models.
'''

import torch

class Loss(torch.nn.Module):
    # ===========================================================================================
    def __init__(self, configs=""):

        super().__init__()

        ## Get the considered experiement: LHCb / ATLAS / CMS...
        Exp = configs['Experiment']
        
        ## epsilon is a parameter that can be adjusted.
        self.epsilon     = configs["training_configs"]["epsilon_loss"]
        ## asym_coeff adjust asymmetry; 1.0 <==> symmetric
        self.asym_coeff  = configs["training_configs"]["asym_coeff"]
        self.model_class = configs["model_class"]
        
        ## nBins is used for normalisation of the loss and depends if using full KDE or intervals
        if self.model_class=="tracks-to-KDE" or self.model_class=="tracks-to-hist":
            self.nBins = configs['global_configs'][Exp]["nBinsPerInterval"]
        else:
            self.nBins = configs['global_configs'][Exp]["n_bins_poca_kde"]
            
        self.verbose = False

        tag_func = {"tracks-to-KDE": "built in Chi^2",
                    "KDE-to-hist":   "built in -log(2r/(r^2 + 1)) with asym parameter",
                    "tracks-to-hist":"built in -log(2r/(r^2 + 1)) with asym parameter"}
            
        ## *********************************************
        print("*"*100)
        print("Initializing the Loss using %s function"%(tag_func[self.model_class]))
        print("")
        print("with the following parameters:\n")
        print("   - Epsilon    =",self.epsilon)
        if self.model_class=="KDE-to-hist" or self.model_class=="tracks-to-hist":
            print("   - Asym coeff =",self.asym_coeff)
        print("   - nBins (for normalisation)    =",self.nBins)
        print("")
        print("*"*100)
        ## *********************************************
        
    # ===========================================================================================
    def forward(self, x, y):

        # --------------------------------------------------------------------------------
        nBatch = y.shape[0]
        if self.verbose:
            print("nBatch = ", nBatch)
            print("y.shape = ",y.shape)
            print("x.shape = ".x.shape)

        '''
        # *******************************************
        NOTE THAT THIS NOTE TESTED AND ONE SHOULD BE 
        VERY CAUTIOUS WHEN USING KDE-to-hist models!!
        '''
        if self.model_class=="KDE-to-hist":
            y = y.transpose(1,2) 
            y = y[:,:,0] ## Get only the KDE, not the X_max, Y_max
        '''
        # *******************************************
        '''


        # --------------------------------------------------------------------------------
        # In the current implementation, not nan shoudl appear as only create target hist for valid PVs 
        # Nevertheless, we keep this implementation as at worst it should do nothing...
        #
        # Make a boolean mask of non-nan values of the target histogram.
        # This will be used to select items from y:
        # see https://docs.scipy.org/doc/numpy-1.13.0/user/basics.indexing.html#boolean-or-mask-index-arrays
        #
        # Note that if masking was not requested when loading the data, there
        # will be no NaNs and this will be all Trues and will do nothing special.
        valid = ~torch.isnan(y)
        
        # --------------------------------------------------------------------------------
        # Using a loss function which is built to optimise the ratio between 
        # predicted and target histograms with an asymmetry parameter acting
        # like a golbal shift on the predicted shape (shifting up or down).
        # This will result in 
        # - increasing efficiency AND false FP (shifting   up predicted hist)
        # - decreasing efficiency AND false FP (shifting down predicted hist)        
        # --------------------------------------------------------------------------------
        if self.model_class=="KDE-to-hist" or self.model_class=="tracks-to-hist":
            # --------------------------------------------------------------------------------
            # Compute r, only including non-nan values. r will probably be shorter than x and y.        
            r = torch.abs((x[valid] + self.epsilon) / (y[valid] + self.epsilon))

            # --------------------------------------------------------------------------------
            # Compute -log(2r/(r² + 1))
            alpha = -torch.log(2*r / (r**2 + 1))
            alpha = alpha * (1.0 + self.asym_coeff * torch.exp(-r))

            # --------------------------------------------------------------------------------
            # Sum up the alpha values, and divide by the length of x and y. Note this is not quite
            # a .mean(), since alpha can be a bit shorter than x and y due to masking.
            beta      = alpha.sum() / self.nBins
            ave_beta  = beta / nBatch

            return ave_beta
        
        elif self.model_class=="tracks-to-KDE":
            # --------------------------------------------------------------------------------
            # Compute a standard MSE like loss function when training tracks-to-KDE models as
            # this seems better suited for this problem.
            
            sigma     = 0.01
            diff      = torch.sub(x[valid],y[valid])
            diff      = diff/sigma
            chisq     = torch.pow(diff,2)
            chisqNdof = chisq.sum() / self.nBins
            ave_chisqNdof = chisqNdof / nBatch
            
            return ave_chisqNdof
